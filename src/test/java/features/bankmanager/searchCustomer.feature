
Feature: Search customer
  In order to manage customer, I want to have the searching customer feature so that I can search customer easily

  Background:
    Given Open Website https://www.way2automation.com/angularjs-protractor/banking/#/login

  Scenario Outline: Search customer successfully
    When I login bank manager successfully
    When I click customer tab
    And I input key search as <keysearch>
    Then I verify that system is already showed customers have <keysearch>

    Examples:
      | keysearch |
      | p         |
      | %         |
      | *         |
      | t         |

  Scenario Outline: Search customer unsuccessfully
    When I login bank manager successfully
    When I click customer tab
    And I input key search as <keysearch>
    Then I verify that system is not showed customers have <keysearch>

    Examples:
      | keysearch |
      | p         |

